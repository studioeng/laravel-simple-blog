@extends('master')
@section('content')

<!--//
login.blade.php
User login to create new posts
//-->

{{ Form::open(array('class' => 'form-horizontal'), 'POST', 'login') }} <!--// L3 Form::open() function is ordered differently //-->
  <fieldset>
      <div id="legend">
          <legend>Login</legend>
      </div>
      <div class="control-group">
          @if ( Session::has('login_errors') )
            <span class="label label-important warning">Username or Password incorrect</span>
          @endif
          <div class="controls">
              <p>{{ Form::text('email', '', array('placeholder' => 'Email')) }}</p>
          </div>
      </div>
      <div class="control-group">
          <div class="controls">
              <p>{{ Form::password('password', array('placeholder' => 'Password')) }}</p>
          </div>
      </div>
      <div class="control-group">
          <div class="controls">
              <p>{{ Form::submit('Login', array('class' => 'btn btn-success')) }}</p>
          </div>
      </div>
  </fieldset>
  {{ Form::close() }}
@stop
