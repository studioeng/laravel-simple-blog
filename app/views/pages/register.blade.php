@extends('master')
@section('content')

<!--//
register.blade.php
Create new blog user

Todo:
Add validation error reporting
 Username required
 Email required
 Password required

 Username taken
 Email taken
//-->

<!--// create form //-->
{{ Form::open(array('class' => 'form-horizontal'), 'POST', 'register') }} <!--// L3 Form::open() function is ordered differently //-->
  <fieldset>
      <div id="legend">
          <legend>Register</legend>
      </div>
      <div class="control-group">
          @if ( Session::has('login_errors') )
            <span class="label label-important warning">Username or Password incorrect</span>
          @endif
          <div class="controls">
                <!--// username field -> required see routes.php //-->
                <p>{{ Form::text('username', '', array('class' => 'span3', 'placeholder' => 'Username')) }}</p>
          </div>
      </div>
      <div class="control-group">
          <div class="controls">
                <!--// email field -> required see routes.php //-->
                <p>{{ Form::text('email', '', array('class' => 'span3', 'placeholder' => 'Email')) }}</p>
          </div>
      </div>
      <div class="control-group">
          <div class="controls">
                <!--// password field -> required see routes.php //-->
                <p>{{ Form::password('password', array('class' => 'span3', 'placeholder' => 'Password')) }}</p>
          </div>
      </div>
      <div class="control-group">
          <div class="controls">
                <!--// submit button //-->
                <p>{{ Form::submit('Register', array('class' => 'btn btn-warning')) }}</p>
          </div>
      </div>
  </fieldset>
  <!--// form end //-->
  {{ Form::close() }}

@stop
