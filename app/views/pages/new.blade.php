@extends('master')
@section('content')

<!--//
new.blade.php
Create new blog post
//-->

{{ Form::open(array('class' => 'form-horizontal'), 'POST', 'admin') }} <!--// L3 Form::open() function is ordered differently //-->
    {{ Form::hidden('author_id', $user->id) }}
    <div class="control-group">
      <!--// post title //-->
      <div class="controls">
        <p>{{ Form::label('title', 'Title') }}</p>
        <!--// field required -> routes.php //-->
        {{ $errors->first('title', '<p class="label label-important warning">The Title is required!</p>') }}
        <p>{{ Form::text('title', Input::old('title')) }}</p>
      </div>
    </div>
    <div class="control-group">
      <!--// post body //-->
      <div class="controls">
        <p>{{ Form::label('body', 'Body') }}</p>
        <!--// field required -> routes.php //-->
        {{ $errors->first('body', '<p class="label label-important warning">The Body is required!</p>') }}
        <p>{{ Form::text('body', Input::old('body')) }}</p>
      </div>
    </div>
    <div class="control-group">
        <!--// submit form //-->
        <div class="controls">
            <p>{{ Form::submit('Create Post', array('class' => 'btn btn-primary')) }}</p>
        </div>
    </div>
{{ Form::close() }}

@stop
