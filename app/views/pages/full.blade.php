@extends('master')
@section('content')

<!--//
full.blade.php
Display full blog post

Todo:
Show post author

Known Bugs:
Will generate an error if no $post->id has been passed to it. - FIXED see routes.php
Will generate an error if $post-id is not valid, doesnt exist in db.
//-->

<div class="span8">
    <h1>{{ HTML::link('view/' . $post->id, $post->title) }}</h1>
    <p>{{ $post->body }}</p>
    <p>{{ HTML::link('/', 'Home', array('class' => 'btn')) }}</p>
    <div>
        <span class="badge badge-success">Posted: {{ $post->created_at }}</span>
    </div>
</div>

@stop
