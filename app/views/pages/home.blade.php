@extends('master')
@section('content')

<!--//
home.blade.php
Display main index page with sample posts in descending order, see routes.php

Todo:
Show post author
//-->

@foreach ( $posts as $post )
    <div class="row">
        <div class="span8">
            <div class="row span8">
                <h4><strong>{{ HTML::link('view/' . $post->id, $post->title) }}</strong> by {{ $post->author_id }}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span2">
            <a href="#" class="thumbnail">
                <img src="http://placehold.it/260x180/666/fff&text={{ $post->author_id }}" alt="posted by {{ $post->author_id }}">
            </a>
        </div>
        <div class="span8">
            <p>
              {{ substr($post->body, 0, 120) . '[...]' }} <!--// Show first 120 characters as preview //-->
            </p>
            <p>{{ HTML::link('view/' . $post->id, 'Read More', array('class' => 'btn')) }}
        </div>
    </div>
@endforeach
{{ $posts->links(); }}

@stop
