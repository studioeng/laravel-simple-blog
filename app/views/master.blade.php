<!--//
L4 uses extends() on child pages
L3 uses layout() on child pages

L3 & L4 uses section()
L3 uses endsection
L4 uses stop
//-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Simple Blog using Laravel 4</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/style.css') }}
    {{ HTML::script('js/bootstrap.js') }}
</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            {{ HTML::link('/', 'Simple Blog', array('class' => 'brand')) }}
        </div>
        <ul class="nav">
            @if ( Auth::guest() )
                <li>{{ HTML::link('admin', 'Login') }}</li>
                <li>{{ HTML::link('register', 'Register') }}</li>
            @else
                <li>{{ HTML::link('admin', 'Admin') }}</li>
                <li>{{ HTML::link('logout', 'Logout') }}</li>
            @endif
        </ul>
    </div>
    <div class="container">
        <h1>Simple Blog</h1>
        <em>A simple blog using Laravel 4</em>
    </div>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>
