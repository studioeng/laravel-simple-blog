<?php
// Index page.
Route::get('/', function() {
	$per_page = 3;
	$posts = Post::orderBy('id', 'desc')->paginate($per_page); // L3 uses order_by().
	return View::make('pages.home')
		->with('posts', $posts);
});
// Fix for having no {post} variable - Redirect back to Index.
// Possible fix could be to find first post in db and display instead.
Route::get('view', function() {
   	return Redirect::to('/');
});
// Show complete blog post {post}=>$post.
Route::get('view/{post}', function($post) {
// known bug: if {post} is left blank generates an error.
   	$post = Post::find($post);
   	return View::make('pages.full')
        ->with('post', $post);
});
// Allow new post if authorised.
Route::get('admin', array('before' => 'auth', 'do' => function() {
	$user = Auth::user();
	return View::make('pages.new')
        ->with('user', $user);
}));
// Validate new post and save to db.
Route::post('admin', array('before' => 'auth', 'do' =>  function() {
    $title = Input::get('title');
    $body = Input::get('body');
    $author_id = Input::get('author_id');

	$new_post = array(
		'title' => $title,
		'body' => $body,
		'author_id' => $author_id,
		);

	$rules = array(
		'title' => 'required|min:3|max:128',
		'body' => 'required'
		);

	$validate = Validator::make($new_post, $rules);

	if ( $validate->fails() ) {
    	return Redirect::to('admin')
			->with('user', Auth::user())
			->withErrors($validate) // L3 uses with_errors($v).
			->withInput(); // L3 uses with_input().
	}

	$post = new Post($new_post);
	$post->save(); // Save new post to db.

	return Redirect::to('view/' .$post->id);
}));
// Show login page.
Route::get('login', function() {
	return View::make('pages.login');
});
// Validate user login.
Route::post('login', function() {
	$userdata = array(
		'email' => Input::get('email'),
		'password' => Input::get('password'),
		);

	if ( Auth::attempt($userdata) ) {
		return Redirect::to('admin');
	} else {
		return Redirect::to('login')
			->with('login_errors', true);
	}

});
// Show register page.
Route::get('register', function() {
	return View::make('pages.register');
});
// Validate and create new user.
Route::post('register', function() {
	$input = Input::all();

	$rules = array(
		'username' => 'required|unique:users',
		'email' => 'required|unique:users|email',
		'password' => 'required'
		);

	$validate = Validator::make($input, $rules);

	if ( $validate->fails() ) {
		return Redirect::to('register')
            ->withErrors($validate); // L3 uses with_errors($v).
	} else {
		$password = $input['password'];
		$password = Hash::make($password);

		$user = new User;
		$user->username = $input['username'];
		$user->email = $input['email'];
		$user->password = $password;
		$user->save();

		return Redirect::to('login');
	}
});
// Logout and return to Index.
Route::get('logout', function() {
	Auth::logout();
	return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function() {
	return Response::error('404');
});

Event::listen('500', function() {
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function() {
	// Do stuff before every request to your application...
});

Route::filter('after', function($response) {
	// Do stuff after every request to your application...
});

Route::filter('csrf', function() {
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function() {
	if (Auth::guest()) return Redirect::to('login');
});
