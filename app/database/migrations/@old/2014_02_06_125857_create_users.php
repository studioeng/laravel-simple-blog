<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Make changes to the database.
	 * Create the users table in the database and insert a record that stores admin's credentials.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('username', 128);
			$table->string('email', 128);
			$table->string('password', 64);
			$table->timestamps();
		});

		DB::table('users')->insert(array(
			'username' => 'admin',
            'email' => 'admin@example.com',
			'password' => Hash::make('password'),
			'name' => 'Admin',
		));
	}

	/**
	 * Revert the changes to the database.
	 * In this case we just drop the table.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
